<?php

use Illuminate\Http\Request;
 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
    Route::post('testing' , 'AuthController@testing')->middleware('cors');
 Route::post('login', 'AuthController@login')->middleware('cors');
 Route::post('register', 'AuthController@register')->middleware('cors');
Route::post('logout', 'AuthController@logout')->middleware('cors');
Route::post('refresh', 'AuthController@refresh')->middleware('cors');
Route::post('me', 'AuthController@me')->middleware('cors');
Route::post('register', 'AuthController@register')->middleware('cors');

/*============================Todo====================================*/

Route::apiResource('registration','RegistrationController');
Route::post('register' , 'AuthController@register');
Route::post('checkEmail' , 'RegistrationController@CheckEmail');
Route::middleware(['jwt_verify'])->group(function () {
    Route::post('basic' , 'ProfileController@basic');
    Route::get('get_basic' , 'ProfileController@get_basic');
    Route::post('Basic_info' , 'ProfileController@Basic_info');
    Route::post('Edu_info' , 'ProfileController@Edu_info');
    Route::get('get_edu' , 'ProfileController@get_edu');
    Route::post('work_info' , 'ProfileController@Work_info');
    Route::post('userImage' , 'ImageController@store');
    Route::get('get_auth' , 'ImageController@get_auth');
    Route::post('interest' , 'ProfileController@interest');
    Route::get('get_Image' , 'ImageController@get_Image');
    Route::get('get_all_images' , 'ImageController@get_all_images');
    Route::post('addPost' , 'PostController@store');
    Route::get('get_posts' , 'PostController@index');
    Route::get('showData' , 'PostController@showData');
    Route::get('get_friend_Image' , 'ImageController@get_friend_Image');
    Route::post('get_friend_info' , 'ProfileController@get_friend_info');
    Route::post('addFriendCustom' , 'ProfileController@addFriendCustom');
    Route::post('get_notifications' , 'NotificationController@get_notifications');
    Route::get('get_user_info' , 'ProfileController@get_user_info');
    Route::post('post_action/{post_id}' , 'PostController@post_action');
    Route::get('all_comments' , 'PostController@all_comments');
    Route::post('all_friends' , 'ProfileController@all_friends');
    Route::post('changePassword' , 'ProfileController@changePassword');
    Route::post('get_user-notification_info/{id}' , 'ProfileController@get_user_notification_info');
    Route::post('friend_request' , 'ProfileController@friend_request');
    Route::post('get_add_friend' , 'ProfileController@get_add_friend');
    Route::get('get_newsFeed' , 'PostController@get_newsFeed');
    Route::post('add_comments' , 'PostController@add_comments');
    Route::post('accept_Request/{id}' , 'ProfileController@accept_Request');
    Route::get('send' , 'ChatController@chat');
    Route::post('send_message' , 'ChatController@send_message');
    Route::get('get_user' , 'ChatController@get_user');
    Route::get('get_friends' , 'ChatController@get_friends');
    Route::get('getUserMessages/{id}' , 'ChatController@getUserMessages');
    Route::get('get_user_newsfeed_similar_data' , 'PostController@get_user_newsfeed_similar_data');
    Route::get('get_interest' , 'ProfileController@get_interest');
    Route::post('interestDelete/{id}' , 'ProfileController@interestDelete');
    Route::post('liked' , 'PostController@liked');
    Route::post('deleteComment/{id}' , 'PostController@deleteComment');
    Route::get('who_to_follow' , 'ProfileController@who_to_follow');
    Route::post('delPost/{id}' , 'PostController@delPost');
    Route::get('activity-log' , 'ActivityLogController@getLogs');
    Route::post('get_friend_follower' , 'ProfileController@get_friend_follower');
    Route::post('get_friends_posts' , 'PostController@get_friends_posts');
    Route::post('all_friends_comments' , 'PostController@all_friends_comments');
    Route::post('searchFriend' , 'PostController@searchFriend');
//'cors',
//Route::post('addTodo' , 'TodoController@addTodo');
//Route::get('getTodo', 'TodoController@getTodoList');
//Route::post('DeleteTodo' , 'TodoController@DeleteTodo');
//Route::post('EditRecord' , 'TodoController@EditRecord');
//Route::post('UpdateTodo' , 'TodoController@UpdateTodo');
//

});
