

<li>{{ $category->category_name }}</li>
@if (count($category['children']) > 0)
    <ul>
        @foreach($category['children'] as $category)
            @include('partials.loop', $category)
        @endforeach
    </ul>
    @else

@endif


{{--{{ $category['children']  }}--}}