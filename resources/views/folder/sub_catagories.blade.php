

<h1>Catagories</h1>



@if (count($categories) > 0)
    <ul>
        @foreach ($categories as $category)
            {{--{{ $category }}--}}
            @include('partials.loop', $category)
        @endforeach
    </ul>
@else
    @include('partials.no_loop')
@endif

