<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catagory extends Model
{
	public function parent()
    {
        return $this->belongsTo(static::class, 'id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }





}
