<?php

namespace App\Http\Controllers;

use App\Notifications\NotifyAddFriend;
use App\UserImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Profile;
use App\UserInterest;
use App\Post;
use App\Friends;
use Psy\Util\Json;
use Hash;
use Notification;
use JWTAuth;
use Illuminate\Support\Facades\Validator;

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileController extends Controller
{
    public function topHeader()
    {
        $authUser = JWTAuth::user();
        return view('layouts.app' , ['id' => $authUser]);
    }
     public function Basic_info(Request $request)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $user = User::where('id' , $id)
            ->update(['f_name' => $request->f_name , 'l_name' => $request->l_name ,
                    'email' => $request->email , 'date' => $request->date ,
                    'month' => $request->month , 'year' => $request->year ,
                    'gender' => $request->gender ,
                    'city' => $request->city , 'country'=>$request->country , 'description'=>$request->description]);

        if ($user) {
        	 $result = User::where('id' , $id)->first();
        } else{
       		$result = "Something wents gone";
        }
            return response()->json($result);
    }
    public function get_basic()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
    	$result = User::where('id' , $id)->first();
    	return response()->json($result);

    }

    public function get_edu()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
    	$result = Profile::where('user_id' , $id)->limit(5)->get();
    	return response()->json($result);
    }
    public function Edu_info(Request $request)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $user = Profile::where('user_id', $id)->count();
        if ($user == 0)
        {
            $user = new Profile;
            $user->user_id = $id;
            $user->university = $request->university;
            $user->edu_from = $request->edu_from;
            $user->edu_to = $request->edu_to;
            $user->edu_description = $request->edu_description;
            $user->graduate = $request->graduate;
            $user->save();
            $user = 0;
        }else{
            $user = Profile::where('user_id' , $id)
            ->update(['university'=> $request->university, 'edu_from'=> $request->edu_from,
                'edu_to'=> $request->edu_to, 'edu_description'=> $request->edu_description, 'graduate'=>$request->graduate]);
            $user = 1;
        }
        return response()->json($user);
    }
    public function Work_info(Request $request )
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $user = Profile::where('user_id', $id)->count();
        if ($user == 0) {
            $user = new Profile;
            $user->user_id = $id;
            $user->work_company = $request->work_company;
            $user->work_designation = $request->work_designation;
            $user->work_from = $request->work_from;
            $user->work_to = $request->work_to;
            $user->work_city = $request->work_city;
            $user->work_description = $request->work_description;
            $user->save();
            $user = 0;
        }else{
            $user = Profile::where('user_id', $id)
            ->update(['work_company' => $request->work_company, 'work_designation'=> $request->work_designation,
                'work_from' => $request->work_from , 'work_to' => $request->work_to ,
                'work_city' => $request->work_city , 'work_description'=>$request->work_description
                ]);
            $user = 1;
        }
        return response()->json($user);
    }
    public function interest(Request $request )
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $interest = $request->user_interest;
        $interst_count = UserInterest::where('user_id' , $id)->count();
        if ($interst_count < 5){
            $query = UserInterest::create(['user_id'=>$id , 'interest'=>$interest]);
            $getInterest = UserInterest::where('user_id' , $id)->get();
            return response()->json(['interests' => $getInterest]);
        }else{
            return response()->json(['error' => 500]);
        }
    }
    public function get_interest()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $getInterest = UserInterest::where('user_id' , $id)->get();
        return response()->json(['interests' => $getInterest]);
    }
    public function interestDelete($id){

        $authUser = JWTAuth::parseToken()->authenticate();
        $auth = $authUser->id;
        $delete = UserInterest::where('id' , $id)->delete();
        $getInterest = UserInterest::where('user_id' , $auth)->get();
        return response()->json(['interests' => $getInterest]);
    }
     public function get_friend_info(Request $request)
     {
            $id = $request->user_id;
            $user = User::where('id' , $id)->first();
            $profile =Profile::where('user_id' , $id)->first();
            $userImg =UserImage::where('user_id' , $id)->get();
            $interest =UserInterest::where('user_id' , $id)->get();
            $posts = Post::where('user_id' , $id)->get();
            $profileImg = UserImage::where('user_id' , $id)->orderBy('created_at', 'desc')->first();
            if (is_null($profileImg)){
                $profileImg = "dummy.jpg";
            }else{
                $profileImg = $profileImg->image;
            }
            return response()->json(['user'=>$user , 'profile'=>$profile , 'userImg'=>$userImg,
            'userInterest'=>$interest , 'posts'=>$posts , 'profileImg'=>$profileImg
            ]);
     }
    public function addFriendCustom(Request $request)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $sender_id = $authUser->id;
        $find = Friends::where([['sender_id' ,$sender_id] , ['receiver_id', $request->user_id]])->orWhere([['receiver_id' ,$sender_id] , ['sender_id', $request->user_id]])->first();
        if(!is_null($find)){
            if($request->status < 0){
                $find->delete(); //unfriend
                return response()->json(5);
            }else{
                $find->status = abs($request->status);        
                $find->save();
            }
        }else{
            $find = Friends::create([
            'sender_id' => $sender_id,
            'receiver_id' => $request->user_id,
            'isFriends' => '',
            'status' => 0 ]);
        }
        return response()->json($find->status);
    }
    public function accept_Request($id)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $auth = $authUser->id;
         $accept = Friends::orWhere('sender_id' , $id)->orWhere('receiver_id' , $auth)->get();
         dd($accept);
    }

    public function get_add_friend(Request $request){
        $user_id = $request->user_id;
         $authUser = JWTAuth::parseToken()->authenticate();
        $sender_id = $authUser->id;
        $find = Friends::where([['sender_id' ,$sender_id] , ['receiver_id', $user_id]])->orWhere([['receiver_id' ,$sender_id] , ['sender_id', $user_id]])->first();
        if(!is_null($find)){
            $status = $find->status;
        }else{            
            $status = 5;
        }
        return response()->json($status);
    }
    public function get_friend_follower(Request $request)
    {
        $followers = Friends::where('sender_id' , $request->user_id)->orWhere('receiver_id' , $request->user_id)->count();
        return response()->json($followers);
    }
     public function get_user_info(){
         $authUser = JWTAuth::parseToken()->authenticate();
         $id = $authUser->id;
         $user = User::where('id' , $id)->first();
        $profile =Profile::where('user_id' , $id)->first();
        $interest =UserInterest::where('user_id' , $id)->get();
         return response()->json(['user'=>$user , 'profile'=>$profile ,
             'userInterest'=>$interest
         ]);
     }
     public function all_friends()
     {
         $authUser = JWTAuth::parseToken()->authenticate();
         $id = $authUser->id;
         $sender = Friends::where('sender_id' , $id)->where('status' , 1)->pluck('receiver_id')->toArray();
         $receiver = Friends::where('receiver_id' , $id)->where('status' , 1)->pluck('sender_id')->toArray();
         $merge = array_merge($sender , $receiver);
            $allFriends = User::with('user_image')->whereIn('id', $merge)->get();
            $arr = array();
            foreach ($allFriends as $name) {
                $n = $name->f_name;
                $arr[] .= $name->id;
            }
       $profile =    Profile::whereIn('user_id' , $arr)->get();
         return json_encode(['all_friends' => $allFriends, 'Profiles'=>$profile ]);
     }
     public function changePassword(Request $request)
    {
        $user = Auth::user();
        $old_pass = $request->old_pass;
        $new_pass = $request->new_pass;
        $confirm_pass = $request->confirm_pass;
        $data = array('password'=>$new_pass , 'password_confirmation'=>$confirm_pass , 'old_pass'=> $old_pass);
        $validate = Validator::make($data, [
            'password' => [
                                'required', 
                                'min:6','confirmed',
                                'max:50',
                                'regex:/^(?=.*[a-z|A-Z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                            ], // asdASD123,./
             ]);
        if ($validate->fails()) {
           return response()->json(["result"=> 1 ]);   
        }
        if (Hash::check($old_pass, $user->password) ) {
            $user_id = $user->id;
            $obj_user = User::find($user_id)->first();
            $obj_user->password = Hash::make($new_pass);
            $obj_user->save();
            return response()->json(["result"=>2]);
        }else{
            return response()->json(["result"=>0]);
        }
    }
    public function friend_request() {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $req = Friends::where(['sender_id' => $id ])->get();
        return response()->json($req);
    }
    public function who_to_follow(){
	    $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
	     $check_friends1 = Friends::where(['sender_id'=>$id , 'status'=>1])->pluck('receiver_id')->toArray();
        $check_friends2 = Friends::where(['receiver_id'=>$id , 'status'=>1])->pluck('sender_id')->toArray();
        $friends = array_merge($check_friends1 , $check_friends2);
        $auth_push = array($id);
        $merge = array_merge($friends , $auth_push);
       $users_ids = User::whereNotIn('id' , $merge)->limit(5)->with('user_image')->get();
       return response()->json($users_ids);
    }
}
