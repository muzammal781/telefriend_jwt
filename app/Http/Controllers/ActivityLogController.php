<?php

namespace App\Http\Controllers;

use App\LogHistory;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
class ActivityLogController extends Controller
{
    public function getLogs()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $user = User::find($id);
        $log = LogHistory::whereUserId($id)->latest()->take(5)->get();
        return response()->json(['log'=> $log , 'username'=> $user->f_name] , 200);
    }
}
