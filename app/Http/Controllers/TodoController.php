<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\User;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Todo;

class TodoController extends Controller
{

	public function getTodoList(Request $request){

		$authUser = JWTAuth::parseToken()->authenticate();
		$id = $authUser->id;
		$getTodo = Todo::where('user_id' , $id)->OrderBy('created_at' , 'desc')->get();

		return response()->json($getTodo);
		
	}

	public function addTodo(Request $request){

			if ($request->todo != null) {
				$authUser = JWTAuth::parseToken()->authenticate();
				$id = $authUser->id;

				$addTodo = Todo::create([
						'user_id' => $id,
						'todo' => $request->todo
				]);
				if ($addTodo) {
					$getTodo = Todo::where('user_id' , $id)->OrderBy('created_at' , 'desc')->get();
					return response()->json($getTodo);
				}
			}
		 
	}
	public function DeleteTodo(Request $request)
	{
		$authUser = JWTAuth::parseToken()->authenticate();
		$auth = $authUser->id;

		$delete = Todo::where('id' , $request->id)->delete();
		if ($delete) {
		 	$getTodo = Todo::where('user_id' , $auth)->OrderBy('created_at' , 'desc')->get();
			return response()->json($getTodo);
		 } 
	}

	public function EditRecord(Request $request){
		$getTodo = Todo::where('id' , $request->id)->first();
		return response()->json($getTodo);
	}

	public function UpdateTodo(Request $request)
	{
		$authUser = JWTAuth::parseToken()->authenticate();
		$auth = $authUser->id;

		$id = $request->id;

		$todo_obj = Todo::findOrFail($id);;

		 $todo_obj->user_id = $auth;
		 $todo_obj->todo = $request->todo;
        $save = $todo_obj->save();

        if ($save) {
        	$getTodo = Todo::where('user_id' , $auth)->OrderBy('created_at' , 'desc')->get();
        	return response()->json($getTodo);
        }

	}

	public function auth_check()
	{
		   return response()->json(JWTAuth::parseToken()->check());
	}


}