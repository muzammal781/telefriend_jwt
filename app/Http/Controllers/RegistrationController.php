<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Test;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function submitData(Request $request)
    {
        $req = $request->name;
        $query = Test::create(['name'=>$req]);
        if ($query) {
            return response()->json(['success'=>'Data is saved']);
        }else{
            return response()->json(['error'=>'some problem']);
        }
    }
    public function showData()
    {
        $query = Test::all();
        return response()->json($query);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
            'email'    => 'unique:users|required',
            'password' => 'required',
            'date' => 'required',
            'month' => 'required',
            'year' => 'required'
        ];

         $input = $request->only('f_name',  'l_name', 'email','password' , 'date' , 'month' ,'year', 'gender');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()]);
        }
        $f_name = $request->f_name;
        $l_name = $request->l_name;
        $email    = $request->email;
        $password = $request->password;
        $date = $request->date;
        $month = $request->month;
        $year = $request->year;
        $gender = $request->gender;
      $user = User::create([
            'f_name' => $f_name,
            'l_name' => $l_name,
            'email' => $email,
            'password' => Hash::make($password),
            'date' => $date,
            'month' => $month,
            'year' => $year,
             'gender' => $gender
        ]);
            return response()->json(['success'  => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CheckEmail(Request $request)
    {
        $email = $request->email_val;
          $user = User::where('email', $email)->count();
        if ($user == 0){
            $result = 'done';
        }else{
            $result = 'exist';
        }
        return json_encode($result);
    }
}
