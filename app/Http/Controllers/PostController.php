<?php

namespace App\Http\Controllers;

use App\LogHistory;
use App\UserImage;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\PostActions;
use App\Friends;
use App\Events\CommentEvent;
use JWTAuth;
use Pusher;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $posts = Post::where('user_id' , $id)->orderBy('created_at', 'desc')->get();
          $image = UserImage::where('user_id' , $id)->orderBy('created_at', 'desc')->first();
        return response()->json(['posts'=>$posts , 'userImg'=>$image ]);
    }
    public function showData() 
    {
         $abc = Post::where('user_id' , auth()->user()->id);
         $result = $abc->user;
        return response()->json($result);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $price = $request->price;
       if ($request->hasFile('file')) {
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extention = $request->file('file')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extention;
            $path = $request->file('file')->move('storage/uploads/UserPost', $filenameToStore);
        }else {
            $filenameToStore = null;
        }
            if ($request->caption != "") {
                $caption =  $request->caption; 
            }else{
                $caption = null;
            }
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $addPost = Post::create([
                'user_id' =>  $id,
                'image' => $filenameToStore,
                'caption' => $caption,
                'price' => $price
            ]);
        $user = User::find($id);
        LogHistory::create([
           'firstname'      => $user->f_name,
            'user_id'       => $id,
            'action'        => 'posted a photo'
        ]);
             if ($addPost) {
                 $result = Post::where('user_id' , $id)->orderBy('created_at', 'desc')->get();
             }else{
                $result = array('done' => 0,);
             }
        return response()->json($result);
    }
    public function delPost($id)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $auth = $authUser->id;
            $delete = Post::where(['user_id' => $auth , 'id'=>$id])->delete();
            $posts = Post::where('user_id' , $auth)->orderBy('created_at', 'desc')->get();
            return response()->json($posts);
    }
    public function post_action(Request $request , $id)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $user_image = $authUser->user_image;
        $user_data = $authUser.$user_image;
        event(new CommentEvent($request->comment , $authUser));
        $user_id = $authUser->id;
        $userImg = $authUser->user_images->last();
        $userName = $authUser->f_name;
        $comment = $request->comment;
        $addComment = PostActions::create([
           'action_perform_user_id' => $user_id,
           'model_name' => 'App\PostActions',
           'model_id' => $id,
           'details' => $comment,
           'action' => 3
        ]);
        $commented_user_id = $addComment->action_perform_user_id;
        $user = User::find($user_id);
        LogHistory::create([
           'firstname'      => $user->f_name,
            'user_id'       => $user_id,
            'action'        => "comment On a Post"
        ]);
        if ($addComment){
            $result         = PostActions::where([ 'model_id' => $id])->get();
            $comment_userImage       = User::find($commented_user_id)->user_image;
            $user_fname     = $comment_userImage->user;
        }else{
            $result = "comment is not posted";
        }
        return response()->json(['result'=>$result , 'userImg' => $userImg ,
            'userName'=>$userName , 'comment_userImage'=> $comment_userImage , 'user_fname' => $user_fname]);
    }

    public function all_comments()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $userName = $authUser->f_name;
        $userImg = $authUser->user_images->last();
        $check_friends1 = Friends::where(['sender_id'=>$id , 'status'=>1])->pluck('receiver_id')->toArray();
        $check_friends2 = Friends::where(['receiver_id'=>$id , 'status'=>1])->pluck('sender_id')->toArray();
        $friends = array_merge($check_friends1 , $check_friends2);
        $auth = array($authUser->id);
        $arr_merge = array_merge($friends , $auth);
        $result = PostActions::whereIn('action_perform_user_id' ,$arr_merge )->get();
        return response()->json(['comments' => $result , 'userImg'=> $userImg , 'userName' => $userName]);
    }
    public function all_friends_comments(Request $request)
    {
        $id = $request->user_id;
        $user = User::find($id);
        $user_image = $user->with()->user_image;dd($user_image);
//        $authUser = JWTAuth::parseToken()->authenticate();
//        $id = $authUser->id;
//        $userName = $authUser->f_name;
//        $userImg = $authUser->user_images->last();
//        $query = PostActions::join('users', 'users.id', '=', 'post_actions.action_perform_user_id')
//                ->join('user_images', 'users.id', '=', 'user_images.user_id')
//                ->select('f_name', 'image', 'action_perform_user_id', 'model_id', 'details')
//                ->where('action_perform_user_id', '=', $id)
//                ->where('action', '=', 3)->get();
        $query = PostActions::where('action_perform_user_id' , $id)->get();
//        $query = $query->user;
        dd($query);
        $check_friends1 = Friends::where(['sender_id'=>$id ])->pluck('receiver_id')->toArray();
        $check_friends2 = Friends::where(['receiver_id'=>$id])->pluck('sender_id')->toArray();
        $friends = array_merge($check_friends1 , $check_friends2);
        $auth = array($id);
        dd($friends);
        $arr_merge = array_merge($friends , $auth);
        dd($arr_merge);
        $result = PostActions::whereIn('action_perform_user_id' ,$arr_merge )->get();
//        return response()->json(['comments' => $result , 'userImg'=> $userImg , 'userName' => $userName]);
    }
    public function deleteComment(Request $request , $id)
    {
        $delete = PostActions::where('id' , $id)->delete();
        $post_id = $request->post_id;
        $comments = PostActions::where('model_id' , $post_id)->get();
        return response()->json($comments);
    }

    public function searchFriend(Request $request)
    {
        $search = $request->searchFriend;
        $query = User::query()
            ->where('f_name', 'LIKE', "{$search}%")
            ->first();
        if (is_null($query)){
            $query = null;
        }else{
            $query = $query->id;
        }
        return response()->json(['result' => $query]);
    }
    public function get_newsFeed()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $check_friends1 = Friends::where(['sender_id'=>$id , 'status'=>1])->pluck('receiver_id')->toArray();
        $check_friends2 = Friends::where(['receiver_id'=>$id , 'status'=>1])->pluck('sender_id')->toArray();
        $friends = array_merge($check_friends1 , $check_friends2);
        $posts = Post::whereIn('user_id' , $friends)->orderBy('created_at' , 'desc')->get();
        $pluck_post = Post::whereIn('user_id' , $friends)->pluck('id')->toArray();
        $auth = array( $id);
         $arr_merge = array_merge($friends , $auth);
        $post_action = PostActions::whereIn('action_perform_user_id' ,$arr_merge )->get();
        $userName = auth()->user()->f_name;
        $userImg = auth()->user()->user_images->last();
        return response()->json(['posts'=> $posts , 'post_action'=>$post_action , 'userName'=>$userName , 'userImg'=>$userImg]);
    }

    public function get_friends_posts(Request $request)
    {
        $id = $request->user_id;
        $posts = Post::where('user_id' , $id)->orderBy('created_at', 'desc')->get();
        $image = UserImage::where('user_id' , $id)->orderBy('created_at', 'desc')->first();
        return response()->json(['posts'=>$posts , 'userImg'=>$image ]);
    }
    public function get_user_newsfeed_similar_data()
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $auth_id = $authUser->id;
        $userImg = $authUser->user_images->last();
        $userName = $authUser->f_name;
        $followers = Friends::where('sender_id' , $auth_id)->orWhere('receiver_id' , $auth_id)->
            where('status' , 1)->count();
        return response()->json(['userName' => $userName , 'userImg' => $userImg , 'followers' => $followers]);
    }
    public function add_comments(Request $request)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $comment = $request->comment;
            $add_comment = PostActions::create([
                'action_perform_user_id' => $id ,
                'model_name' => 'App/PostActions',
                'model_id' => $request->post_id,
                'details' => $comment,
                'action' => 3
            ]);
            $user = User::find($id);
        event(new CommentEvent($comment , $user));
        $get_comment = PostActions::where(['action_perform_user_id' => $id , 'model_id' => $request->post_id])->OrderBy('created_at' , 'desc')->first();
         return response()->json($get_comment);
    }
    public function liked(Request $request)
    {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
        $post_id = $request->post_id;
        $liked = PostActions::where(['action_perform_user_id' => $id  , 'model_id' => $post_id ,  'action' => 1])->first();
        if ($liked){
            $deleted = PostActions::where(['action_perform_user_id' => $id  , 'model_id' => $post_id ,  'action' => 1])->delete();
             $count = PostActions::where(['model_id' => $post_id ,  'action' => 1])->count();

        }else{
           $add = PostActions::create([
                'action_perform_user_id' => $id ,
                'model_id' => $post_id ,
                'model_name' => 'App/PostActions',
                'action' => 1 ,
                'details' => 1
            ]);
            $count = PostActions::where(['model_id' => $post_id ,  'action' => 1])->count();
        }
        return response()->json(['post_id' => $post_id , 'likes' => $count]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
