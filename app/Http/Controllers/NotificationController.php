<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request; 
use JWTAuth;
class NotificationController extends Controller
{
    //
    public function get_notifications() {
        $authUser = JWTAuth::parseToken()->authenticate();
        $id = $authUser->id;
    	 $count = auth()->user()->unreadNotifications->count();
    	 $notifications = $authUser->notifications;
      return response()->json(['count' => $count , 'all_notifications' => $notifications]);
    }
}
