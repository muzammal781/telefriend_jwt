<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\User; 
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RegisterAuthRequest;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    public $loginAfterSignUp = true;
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {  
        $credentials = $request->only('email', 'password');
  
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);

    }


    public function register(Request $request) {

        $rules = [
            'f_name' => 'required',
            'l_name' => 'required',
            'email'    => 'unique:users|required',
            'password' => 'required',
            'date' => 'required',
            'month' => 'required',
            'year' => 'required'
        ];

        $input = $request->only('f_name',  'l_name', 'email','password' , 'date' , 'month' ,'year', 'gender');

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()]);
        }

        $f_name = $request->f_name;
        $l_name = $request->l_name;
        $email    = $request->email;
        $password = $request->password;
        $date = $request->date;
        $month = $request->month;
        $year = $request->year;
        $gender = $request->gender;

        //$user = array('f_name'=>$f_name ,'l_name'=> $l_name,'email'=>$email,'password'=>$password);

        $user = User::create([
            'f_name' => $f_name,
            'l_name' => $l_name,
            'email' => $email,
            'password' => Hash::make($password),
            'date' => $date,
            'month' => $month,
            'year' => $year,
            'gender' => $gender
        ]);

            return response()->json(['success'  => true]);

    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    { 
        
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}