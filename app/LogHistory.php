<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogHistory extends Model
{
    protected $fillable = [
        'firstname', 'action', 'user_id'
    ];
}
